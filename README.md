# generic-node

## Summary

This repo contains an example of building a set of generic nodes on OpenStack driven by Ansible.

The basic process to use is as follows:

source your `*-openrc.sh` file

Populate your personal `PrivateRules.mak` with something like:
```
KEY_PAIR = piers-engage
IDENTITY_FILE = ../piers-engage.pem
FLAVOR = m1.medium
```

Look at the `Makefile` vars and modify to taste.

eg:
```
IMAGE_ID
```

Review playbooks/group_vars/node and change `instances:` to reflect the names of nodes to be created.  Change `cluster_prefix` to a value that will be used to prefix all resources created eg: networks, routers, security groups, instances etc.

To build use `make all`, to tear everything down again use `make clean_all`.
