KEY_PAIR ?= your-key
IDENTITY_FILE ?=$(HOME)/.ssh/id_rsa
REGION := nz-por-1
IMAGE_ID := Ubuntu-18.04-x86_64
JUMP_HOST ?= ubuntu@10.128.26.15
FLAVOR := m1.small

.DEFAULT_GOAL := help

# define overides for above variables in here
-include PrivateRules.mak

ifndef KEY_PAIR
    $(error KEY_PAIR has not been supplied eg: make <action> KEY_PAIR=your-key)
endif

all: build_all ## call build_all

# clean_network:
# 	ansible-playbook playbooks/remove-network.yml

clean_sg:  ## destroy secuirty group/s
	ansible-playbook playbooks/remove-security-group.yml -e @playbooks/group_vars/node

clean_node:  ## destroy out nodes
	ansible-playbook -i inventory/static playbooks/remove-node-host.yml -e @playbooks/group_vars/node

clean_hosts: clean_node ## destroy hosts (alias for nodes)

clean_all: clean_hosts clean_sg  ## destroy nodes and security group/s

# build_network:
# 	ansible-playbook playbooks/create-network.yml \
# 	--extra-vars="region=$(REGION) keypair_name=$(KEY_PAIR)"

build_sg:  ## build the security group/s
	ansible-playbook playbooks/create-security-group.yml \
	 -e @playbooks/group_vars/node \
	--extra-vars="region=$(REGION) keypair_name=$(KEY_PAIR)"

build_node:  ## build the nodes (VMs)
	cp -f ./inventory/static.in ./inventory/static
	cp -f ./playbooks/ssh.config.in ./playbooks/ssh.config
	ansible-playbook -i inventory/static  -i inventory/localhost playbooks/create-node-hosts.yml \
	-e @playbooks/group_vars/node \
	--extra-vars="jump_host=$(JUMP_HOST) image=$(IMAGE_ID) region=$(REGION) keypair_name=$(KEY_PAIR) flavor=$(FLAVOR) identityfile=$(IDENTITY_FILE)"

build_hosts: build_sg build_node

build_docker:  ## apply the docker roles
	cd  playbooks && ansible-playbook -i ../inventory/static docker.yml \
	-e @group_vars/node \
	--extra-vars="jump_host=$(JUMP_HOST) image=$(IMAGE_ID) region=$(REGION) keypair_name=$(KEY_PAIR) flavor=$(FLAVOR) identityfile=$(IDENTITY_FILE)"

build_all: build_hosts build_docker  ## build hosts and then apply docker

help:  ## show this help.
	@echo "make targets:"
	@grep -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ": .*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
	@echo ""; echo "make vars (+defaults):"
	@grep -E '^[0-9a-zA-Z_-]+ \?=.*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = " \\?\\= "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
